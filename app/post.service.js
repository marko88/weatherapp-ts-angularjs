System.register(['angular2/http', 'rxjs/add/operator/map', 'angular2/core'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1;
    var PostService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {},
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            PostService = (function () {
                function PostService(_http) {
                    this._http = _http;
                    this._myIPInfoUrl = 'http://ipinfo.io/json';
                    this._weatherId = '41206e6cab9325d6234a81c50b75abc1';
                    this._weatherBaseURL = 'http://api.openweathermap.org/data/2.5/weather?q=';
                    this._weatherGroupBaseURL = 'http://api.openweathermap.org/data/2.5/group?id=';
                    this._arrayOfCityCodes = [
                        '524901',
                        '703448',
                        '2643743',
                        '5128638',
                        '5790179',
                        '3442098',
                        '2172832',
                        '1818097',
                        '2983705',
                        '6535753' // IT San Fedele Intelvi
                    ];
                }
                PostService.prototype.getIPInfo = function () {
                    return this._http.get(this._myIPInfoUrl)
                        .map(function (res) { return res.json(); });
                };
                PostService.prototype.getWeatherForIPInfo = function (ipinfo) {
                    var weatherURL = this._weatherBaseURL + ipinfo.city + ',' + ipinfo.country + '&units=metric&appid=' + this._weatherId;
                    return this._http.get(weatherURL)
                        .map(function (res) { return res.json(); });
                };
                PostService.prototype.getWeatherFor10Cities = function () {
                    var url = this._weatherGroupBaseURL + this._arrayOfCityCodes.join() + '&units=metric&appid=' + this._weatherId;
                    return this._http.get(url)
                        .map(function (res) { return res.json().list; });
                };
                PostService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], PostService);
                return PostService;
            })();
            exports_1("PostService", PostService);
        }
    }
});
//# sourceMappingURL=post.service.js.map