import {Http} from 'angular2/http';
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import {Injectable} from 'angular2/core'
import {IPInfo} from './ipinfo'
import {WeatherInfo} from './weatherinfo'

@Injectable()
export class PostService {
    private _myIPInfoUrl = 'http://ipinfo.io/json';
    
    private _weatherId = '41206e6cab9325d6234a81c50b75abc1';
    private _weatherBaseURL = 'http://api.openweathermap.org/data/2.5/weather?q=';
    private _weatherGroupBaseURL = 'http://api.openweathermap.org/data/2.5/group?id=';
    
    private _arrayOfCityCodes = [
        '524901',  // RU Moscow
        '703448',  // UA Kiev
        '2643743', // GB London
        '5128638', // US New York
        '5790179', // US Clark County
        '3442098', // UY La Paz
        '2172832', // AU Caboolture
        '1818097', // CN Andun
        '2983705', // FR Ribeauville
        '6535753'  // IT San Fedele Intelvi
    ];
    
    constructor(private _http: Http) {}
    
    getIPInfo() : Observable<IPInfo> {
        return this._http.get(this._myIPInfoUrl)
            .map(res => res.json());
    }
    
    getWeatherForIPInfo(ipinfo: IPInfo) : Observable<WeatherInfo> {
        var weatherURL = this._weatherBaseURL + ipinfo.city + ',' + ipinfo.country + '&units=metric&appid=' + this._weatherId;
        return this._http.get(weatherURL)
            .map(res => res.json());
    }
    
    getWeatherFor10Cities() : Observable<WeatherInfo[]> {
        var url = this._weatherGroupBaseURL + this._arrayOfCityCodes.join() + '&units=metric&appid=' + this._weatherId;
        return this._http.get(url)
            .map(res => res.json().list);
    }
    
}