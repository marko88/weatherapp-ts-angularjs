import {Component, OnInit} from 'angular2/core';
import {PostService} from './post.service';
import {HTTP_PROVIDERS} from 'angular2/http'
import {IPInfo} from './ipinfo'
import {WeatherInfo} from './weatherinfo'

@Component({
    selector: 'app',
    templateUrl: 'app/app.template.html',
    providers: [PostService, HTTP_PROVIDERS],
    styleUrls: ['app/app.styles.css']
})
export class AppComponent implements OnInit {
    private _buttonText = 'Get Weather For Current Location';
    private _erroText = '';
    private _isClicked = false;
    private _isLoading = false;
    private _readyForLoading10Cities = false;
    
    private _weatherElement = {
        name: '',
        country: '',
        weather: '',
        weatherDesc: '',
        weatherIconURL: '',
        temp: 0,
    };
    
    private _weatherArray = [];
    
    constructor(private _postService: PostService) {
        this._weatherArray[0] = this._weatherElement;
    }
    
    buttonClick() {
        this._buttonText = 'Acquiring IP addrress';
        this._isLoading = true;
        this._postService.getIPInfo()
            .subscribe(
                ipinfo => {
                    this._isClicked = true;
                    this._isLoading = false;              
                    console.log(ipinfo);
                    this.getLocalWeather(ipinfo);
                },
                error => {
                    this._buttonText = 'Retry';
                    this._erroText = 'Error obtaining IP address';
                    this._isLoading = false;
                    console.error("Error: Obtaining location from ipinfo.io FAILED!");
                    console.error(error);
                }
            );
    }
    
    getLocalWeather(ipinfo: IPInfo) {
        this._buttonText = 'Acquiring Weather Information';
        this._isClicked = false;
        this._isLoading = true;
        this._postService.getWeatherForIPInfo(ipinfo)
            .subscribe(
                weatherinfo => {
                    this._isClicked = true;
                    this._isLoading = false;
                    console.log(weatherinfo);
                    this._weatherElement = {
                        name: weatherinfo.name,
                        country: weatherinfo.sys.country,
                        weather: weatherinfo.weather[0].main,
                        weatherDesc: weatherinfo.weather[0].description,
                        weatherIconURL: 'http://openweathermap.org/img/w/'+weatherinfo.weather[0].icon+'.png',
                        temp: Math.round(weatherinfo.main.temp)
                    };
                },
                error => {
                    this._buttonText = 'Retry';
                    this._erroText = 'Error obtaining weather info';
                    this._isLoading = false;
                    console.error("Error: Obtaining weather from openweathermap.org FAILED!");
                    console.error(error);
                }
            );
    }
    
    ngOnInit() {
        this._postService.getWeatherFor10Cities()
            .subscribe(
                weatherinfoList => {
                    console.log(weatherinfoList);
                    this._weatherArray = []; // erase first empty element
                    for (let weatherinfo of weatherinfoList) {
                        var count = this._weatherArray.length;
                        this._weatherArray[count] = {
                            name: weatherinfo.name,
                            country: weatherinfo.sys.country,
                            weather: weatherinfo.weather[0].main,
                            weatherDesc: weatherinfo.weather[0].description,
                            weatherIconURL: 'http://openweathermap.org/img/w/'+weatherinfo.weather[0].icon+'.png',
                            temp: Math.round(weatherinfo.main.temp)
                        };
                    };
                    this._readyForLoading10Cities = true;
                },
                error => {
                    console.error("Error: Obtaining weather for 10 cities from openweathermap.org FAILED!");
                    console.error(error);
                }
            );
    }
}