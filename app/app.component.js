System.register(['angular2/core', './post.service', 'angular2/http'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, post_service_1, http_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (post_service_1_1) {
                post_service_1 = post_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(_postService) {
                    this._postService = _postService;
                    this._buttonText = 'Get Weather For Current Location';
                    this._erroText = '';
                    this._isClicked = false;
                    this._isLoading = false;
                    this._readyForLoading10Cities = false;
                    this._weatherElement = {
                        name: '',
                        country: '',
                        weather: '',
                        weatherDesc: '',
                        weatherIconURL: '',
                        temp: 0,
                    };
                    this._weatherArray = [];
                    this._weatherArray[0] = this._weatherElement;
                }
                AppComponent.prototype.buttonClick = function () {
                    var _this = this;
                    this._buttonText = 'Acquiring IP addrress';
                    this._isLoading = true;
                    this._postService.getIPInfo()
                        .subscribe(function (ipinfo) {
                        _this._isClicked = true;
                        _this._isLoading = false;
                        console.log(ipinfo);
                        _this.getLocalWeather(ipinfo);
                    }, function (error) {
                        _this._buttonText = 'Retry';
                        _this._erroText = 'Error obtaining IP address';
                        _this._isLoading = false;
                        console.error("Error: Obtaining location from ipinfo.io FAILED!");
                        console.error(error);
                    });
                };
                AppComponent.prototype.getLocalWeather = function (ipinfo) {
                    var _this = this;
                    this._buttonText = 'Acquiring Weather Information';
                    this._isClicked = false;
                    this._isLoading = true;
                    this._postService.getWeatherForIPInfo(ipinfo)
                        .subscribe(function (weatherinfo) {
                        _this._isClicked = true;
                        _this._isLoading = false;
                        console.log(weatherinfo);
                        _this._weatherElement = {
                            name: weatherinfo.name,
                            country: weatherinfo.sys.country,
                            weather: weatherinfo.weather[0].main,
                            weatherDesc: weatherinfo.weather[0].description,
                            weatherIconURL: 'http://openweathermap.org/img/w/' + weatherinfo.weather[0].icon + '.png',
                            temp: Math.round(weatherinfo.main.temp)
                        };
                    }, function (error) {
                        _this._buttonText = 'Retry';
                        _this._erroText = 'Error obtaining weather info';
                        _this._isLoading = false;
                        console.error("Error: Obtaining weather from openweathermap.org FAILED!");
                        console.error(error);
                    });
                };
                AppComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._postService.getWeatherFor10Cities()
                        .subscribe(function (weatherinfoList) {
                        console.log(weatherinfoList);
                        _this._weatherArray = []; // erase first empty element
                        for (var _i = 0; _i < weatherinfoList.length; _i++) {
                            var weatherinfo = weatherinfoList[_i];
                            var count = _this._weatherArray.length;
                            _this._weatherArray[count] = {
                                name: weatherinfo.name,
                                country: weatherinfo.sys.country,
                                weather: weatherinfo.weather[0].main,
                                weatherDesc: weatherinfo.weather[0].description,
                                weatherIconURL: 'http://openweathermap.org/img/w/' + weatherinfo.weather[0].icon + '.png',
                                temp: Math.round(weatherinfo.main.temp)
                            };
                        }
                        ;
                        _this._readyForLoading10Cities = true;
                    }, function (error) {
                        console.error("Error: Obtaining weather for 10 cities from openweathermap.org FAILED!");
                        console.error(error);
                    });
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'app',
                        templateUrl: 'app/app.template.html',
                        providers: [post_service_1.PostService, http_1.HTTP_PROVIDERS],
                        styleUrls: ['app/app.styles.css']
                    }), 
                    __metadata('design:paramtypes', [post_service_1.PostService])
                ], AppComponent);
                return AppComponent;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map